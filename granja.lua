local S = minetest.get_translator(minetest.get_current_modname())

local mcempv = {}
mcempv.cosechadora = {}
mcempv.cosechadora.cajon = {}
mcempv.farming = {}
mcempv.inventario = {}

mcempv.inventario.crear_vacio = function(cant)
    local retornar = {}
    for i=1, cant do
        retornar[i] = ''
    end

    return retornar
end

mcempv.farming.planta = function(nombre_planta)
    local da = {planta = '', semilla = ''}
    if 'farming:hemp'==nombre_planta then
        da.planta = 'farming:hemp_leaf'
    elseif 'farming:mint'==nombre_planta then
        da.planta = 'farming:mint_leaf'
    elseif 'farming:melon'==nombre_planta then
        da.planta = 'farming:melon_8'
    elseif 'farming:raspberry'==nombre_planta then 
        da.planta = 'farming:raspberries'
        nombre_planta = 'farming:raspberries'
    elseif 'farming:blueberry'==nombre_planta then 
        da.planta = 'farming:blueberries'
        nombre_planta = 'farming:blueberries'
    else
        da.planta = nombre_planta
    end
    
    if type(farming.registered_plants[nombre_planta])=='nil' then
        --minetest.log(dump(nombre_planta)..' es nil')
        return {error = true}
    end

    da.semilla = farming.registered_plants[nombre_planta].seed
    if 'farming:sunflower'==nombre_planta then
        da.semilla = ''
    elseif 'farming:pineapple'==nombre_planta then
        da.semilla = ''
    elseif 'farming:pumpkin'==nombre_planta then
        da.semilla = ''
    elseif 'farming:melon'==nombre_planta then
        da.semilla = ''
    elseif 'farming:pepper'==nombre_planta then
        da.semilla = ''
    else
        da.semilla = farming.registered_plants[nombre_planta].seed
    end

    return {error = false,
            planta = nombre_planta,
            semilla = farming.registered_plants[nombre_planta].seed,
            crecimiento = farming.registered_plants[nombre_planta].crop,
            da = {planta = da.planta, semilla = da.semilla}
        }
end


mcempv.farming.semilla = function(nombre_semilla)
    --minetest.log('nombre_semilla: '..nombre_semilla)
    local planta = ''
    if nombre_semilla=='farming:seed_wheat' then
        planta = 'farming:wheat'
    elseif nombre_semilla=='farming:wheat' then
        planta = ''
    elseif nombre_semilla=='farming:seed_cotton' then
        planta = 'farming:cotton'
    elseif nombre_semilla=='farming:cotton' then
        planta = ''
    elseif nombre_semilla=='farming:seed_barley' then
        planta = 'farming:barley'
    elseif nombre_semilla=='farming:barley' then
        planta = ''
    elseif nombre_semilla=='farming:seed_hemp' then
        planta = 'farming:hemp'
    elseif nombre_semilla=='farming:seed_sunflower' then
        planta = 'farming:sunflower'
    elseif nombre_semilla=='farming:seed_mint' then
        planta = 'farming:mint'
    elseif nombre_semilla=='farming:pineapple_top' then
        planta = 'farming:pineapple'
    elseif nombre_semilla=='farming:pineapple' then
        planta = ''
    elseif nombre_semilla=='farming:seed_oat' then
        planta = 'farming:oat'
    elseif nombre_semilla=='farming:oat' then
        planta = ''
    elseif nombre_semilla=='farming:seed_rye' then
        planta = 'farming:rye'
    elseif nombre_semilla=='farming:rye' then
        planta = ''
    elseif nombre_semilla=='farming:pumpkin_slice' then
        planta = 'farming:pumpkin'
    elseif nombre_semilla=='farming:pumpkin' then
        planta = ''
    elseif nombre_semilla=='farming:melon_slice' then
        planta = 'farming:melon'
    elseif nombre_semilla=='farming:melon' then
        planta = ''
    elseif nombre_semilla=='farming:peppercorn' then
        planta = 'farming:pepper'
    elseif nombre_semilla=='farming:pepper' then
        planta = ''
    elseif type(farming.registered_plants[nombre_semilla])~='nil' then
        planta = nombre_semilla
    else
        planta = ''
    end
    --minetest.log('planta: '..planta)
    if planta=='' then
        return {error = true}
    else
        return mcempv.farming.planta(planta)
    end
end


mcempv.farming.crecimiento_terminado = function(nombre_crecimiento)
    if type(farming.plant_stages[nombre_crecimiento])=='nil' then
        return {planta = false,
                cosechar = false}
    end

    local nombre_planta = farming.plant_stages[nombre_crecimiento].plant_name
    if mcempv.farming.planta(nombre_planta).error then
        --minetest.log('Error en la planta 122')
        return {planta = false,
                cosechar = false}
    end 
    if #farming.plant_stages[nombre_crecimiento].stages_left==0 then
        return {datos = mcempv.farming.planta(nombre_planta),
                planta = true,
                cosechar = true}
    else
        return {planta = true,
                cosechar = false}
    end
    --return mcempv.farming.planta(nombre_planta)
end

mcempv.cosechadora.infotext = function(pos_cosechadora, texto)
    local fuel = minetest.get_meta(pos_cosechadora):get_int("biofuel")

    if fuel>0 then
        fuel = 100*fuel/300*10
    else
        fuel = 0
    end
    texto = 'Cosechadora: '..texto..' '..(math.round(fuel)/10)..'%'
    minetest.get_meta(pos_cosechadora):set_string("infotext", texto)
    return texto
end


mcempv.cosechadora.get_inventario = function(pos_cosechadora)
    return minetest.get_meta(pos_cosechadora):get_inventory({ type="node", pos=pos_cosechadora })
end

mcempv.cosechadora.inventario_sacar = function(pos_cosechadora, inventario, id, cant)
    item = mcempv.cosechadora.get_inventario(pos_cosechadora):get_stack(inventario, id)
    item:take_item(cant)
    mcempv.cosechadora.get_inventario(pos_cosechadora):set_stack(inventario, id, item)
end


mcempv.cosechadora.direccion_id = function(direccion)
    if direccion=='Norte' then
        return 1
    elseif direccion=='Este' then
        return 2
    elseif direccion=='Sur' then
        return 3
    elseif direccion=='Oeste' then
        return 4
    end
    return 1
end


mcempv.cosechadora.formspec = function(direccion)    
    return "size[14.5,8]"
    .."dropdown[2.9,0;1.2;mcempv_cosechadora_Direccion;Norte,Este,Sur,Oeste;"
    ..mcempv.cosechadora.direccion_id(direccion)..";]"
    .."list[context;biofuel;0,0;1,1;]"
    .."list[context;parcela;0,1;7,2;]"
    .."list[context;cajon;7.5,0;7,4;]"
    .."list[current_player;main;3.3,4.5;8,4;]"
end


mcempv.cosechadora.cajon.meter = function(pos_cosechadora, item, cant)
    if item=='' then
        return true
    end
    if type(cant)=='nil' then
        cant = 1
    end
    if mcempv.cosechadora.get_inventario(pos_cosechadora):add_item('cajon', ItemStack({name = item, count = cant})):get_count()==0 then
        return false
    else
        return true
    end
end


mcempv.cosechadora.tierra_plantacion = function(pos)
    if minetest.get_node(pos).name=="farming:soil" or 
       minetest.get_node(pos).name=="farming:soil_wet" then
        return true
    else
        return false
    end
end


mcempv.cosechadora.cosecha_la_planta = function(pos_cosechadora, pos)
    local planta = mcempv.farming.crecimiento_terminado(minetest.get_node(pos).name)
    local cant = 1
    local item1 = 0
    local item2 = 0
    --minetest.log('mcempv.cosechadora.cosecha_la_planta')
    if planta.cosechar then
        if math.random(3) == 1 then
            cant = 2
        end
        item1 = mcempv.cosechadora.cajon.meter(pos_cosechadora, planta.datos.da.semilla, cant)
        item2 = mcempv.cosechadora.cajon.meter(pos_cosechadora, planta.datos.da.planta)
        if not item1 or not item2 then
            minetest.get_meta(pos_cosechadora):set_int("biofuel", minetest.get_meta(pos_cosechadora):get_int("biofuel")-1)
            minetest.set_node(pos, { name = "air" })
        end
        return false
    elseif planta.planta then
        return true
    else
        return false
    end
end


mcempv.cosechadora.planta_la_semilla = function(pos_cosechadora, pos, x, z)
    local item = {}
    local planta = {}

    if z>4 then
        x = x + 7
    end

    item = mcempv.cosechadora.get_inventario(pos_cosechadora):get_stack('parcela', x)
    planta = mcempv.farming.semilla(item:get_name())
    if planta.error then
        return false -- no es semilla
    elseif minetest.get_node(pos).name=='air' and minetest.get_meta(pos_cosechadora):get_int("biofuel")>0 then
        --item:take_item(1)
        --mcempv.cosechadora.inventario:set_stack('parcela', x, item)
        mcempv.cosechadora.inventario_sacar(pos_cosechadora, 'parcela', x, 1)
        --minetest.place_node(pos, { name = planta.crecimiento..'_1' })
        minetest.add_node(pos, { name = planta.crecimiento..'_1' })
        minetest.get_meta(pos_cosechadora):set_int("biofuel", minetest.get_meta(pos_cosechadora):get_int("biofuel")-1)
        return true -- plantada la semilla
    else
        return false -- no esta vacio el espacio
    end
end


mcempv.cosechadora.plantar = function(pos)
    --local meta = minetest.get_meta(pos)
    local direc = minetest.get_meta(pos):get_string("direccion")
    local pos_cosechadora = {}    
    pos_cosechadora.x = pos.x
    pos_cosechadora.y = pos.y
    pos_cosechadora.z = pos.z

    minetest.get_meta(pos_cosechadora):set_string("mantener_temporizador", 'n')
    --minetest.log('mcempv.cosechadora.plantar')
    if 'Norte'==direc then
        mcempv.cosechadora.plantar_norte(pos_cosechadora, pos)
    elseif 'Sur'==direc then
        mcempv.cosechadora.plantar_sur(pos_cosechadora, pos)
    elseif 'Este'==direc then
        mcempv.cosechadora.plantar_este(pos_cosechadora, pos)
    elseif 'Oeste'==direc then
        mcempv.cosechadora.plantar_oeste(pos_cosechadora, pos)
    else
        mcempv.cosechadora.plantar_norte(pos_cosechadora, pos)
    end
end


mcempv.cosechadora.plantar_norte = function(pos_cosechadora, pos)    
    pos.z = pos.z + 4
    pos.x = pos.x - 4
    local p_x = pos.x
    local hay_planta = false
    local hay_semilla = false
    --minetest.log('mcempv.cosechadora.plantar_norte')
    for z=1, 7 do
        pos.z = pos.z - 1
        for x=1, 7 do
            if 1==x then pos.x = p_x end
            pos.x = pos.x + 1
            if pos.x==pos_cosechadora.x and pos.z==pos_cosechadora.z then
            else
                pos.y = pos.y - 1
                if mcempv.cosechadora.tierra_plantacion(pos) then
                    pos.y = pos.y + 1
                    hay_planta = mcempv.cosechadora.cosecha_la_planta(pos_cosechadora, pos)
                    hay_semilla = mcempv.cosechadora.planta_la_semilla(pos_cosechadora, pos, x, z)
                    if hay_planta or hay_semilla then
                        minetest.get_meta(pos_cosechadora):set_string("mantener_temporizador", 's')
                    end
                else
                    pos.y = pos.y + 1
                end
            end
        end 
    end
end


mcempv.cosechadora.plantar_sur = function(pos_cosechadora, pos)    
    pos.z = pos.z - 4
    pos.x = pos.x + 4
    local p_x = pos.x
    local hay_planta = false
    local hay_semilla = false

    for z=1, 7 do
        pos.z = pos.z + 1
        for x=1, 7 do
            if 1==x then pos.x = p_x end
            pos.x = pos.x - 1
            if pos.x~=pos_cosechadora.x or pos.z~=pos_cosechadora.z then
                pos.y = pos.y - 1
                if minetest.get_node(pos).name=="farming:soil" or 
                   minetest.get_node(pos).name=="farming:soil_wet" then
                    pos.y = pos.y + 1
                    hay_planta = mcempv.cosechadora.cosecha_la_planta(pos_cosechadora, pos)
                    hay_semilla = mcempv.cosechadora.planta_la_semilla(pos_cosechadora, pos, x, z)
                    if hay_planta or hay_semilla then
                        minetest.get_meta(pos_cosechadora):set_string("mantener_temporizador", 's')
                    end
                else
                    pos.y = pos.y + 1
                end
            end
        end 
    end
    --for i,v in ipairs(dias) do
    --    minetest.set_node(pos, { name = "farming:soil_wet" })
    --end
end


mcempv.cosechadora.plantar_este = function(pos_cosechadora, pos)    
    pos.z = pos.z + 4
    pos.x = pos.x + 4
    local p_x = pos.z
    local hay_planta = false
    local hay_semilla = false

    for z=1, 7 do
        pos.x = pos.x - 1
        for x=1, 7 do
            if 1==x then pos.z = p_x end
            pos.z = pos.z - 1
            if pos.x~=pos_cosechadora.x or pos.z~=pos_cosechadora.z then
                pos.y = pos.y - 1
                --if minetest.get_node(pos).name=="farming:soil_wet" then
                if minetest.get_node(pos).name=="farming:soil" or 
                   minetest.get_node(pos).name=="farming:soil_wet" then
                    pos.y = pos.y + 1
                    hay_planta = mcempv.cosechadora.cosecha_la_planta(pos_cosechadora, pos)
                    hay_semilla = mcempv.cosechadora.planta_la_semilla(pos_cosechadora, pos, x, z)
                    if hay_planta or hay_semilla then
                        minetest.get_meta(pos_cosechadora):set_string("mantener_temporizador", 's')
                    end
                else
                    pos.y = pos.y + 1
                end
            end
        end 
    end
    --for i,v in ipairs(dias) do
    --    minetest.set_node(pos, { name = "farming:soil_wet" })
    --end
end


mcempv.cosechadora.plantar_oeste = function(pos_cosechadora, pos)    
    pos.z = pos.z - 4
    pos.x = pos.x - 4
    local p_x = pos.z
    local hay_planta = false
    local hay_semilla = false

    for z=1, 7 do
        pos.x = pos.x + 1
        for x=1, 7 do
            if 1==x then pos.z = p_x end
            pos.z = pos.z + 1
            if pos.x~=pos_cosechadora.x or pos.z~=pos_cosechadora.z then
                pos.y = pos.y - 1
                --if minetest.get_node(pos).name=="farming:soil_wet" then
                if minetest.get_node(pos).name=="farming:soil" or 
                   minetest.get_node(pos).name=="farming:soil_wet" then
                    pos.y = pos.y + 1
                    hay_planta = mcempv.cosechadora.cosecha_la_planta(pos_cosechadora, pos)
                    hay_semilla = mcempv.cosechadora.planta_la_semilla(pos_cosechadora, pos, x, z)
                    if hay_planta or hay_semilla then
                        minetest.get_meta(pos_cosechadora):set_string("mantener_temporizador", 's')
                    end
                else
                    pos.y = pos.y + 1
                end
            end
        end 
    end
    --for i,v in ipairs(dias) do
    --    minetest.set_node(pos, { name = "farming:soil_wet" })
    --end
end


mcempv.cosechadora.on_timer = function(pos)
    if minetest.get_meta(pos):get_string("mantener_temporizador")=='s' and minetest.get_meta(pos):get_int("biofuel")>0 then
        --meta:set_string("infotext", 'Cosechadora: Funcionando')
        mcempv.cosechadora.infotext(pos, 'Funcionando')
        mcempv.cosechadora.plantar(pos)
        mcempv.cosechadora.infotext(pos, 'Funcionando')
        return true
    else
        --meta:set_string("infotext", 'Cosechadora: Parada')
        mcempv.cosechadora.infotext(pos, 'Parada')
        return false
    end
end


mcempv.cosechadora.temporizador_activar = function(pos)
    if not minetest.get_node_timer(pos):is_started() then
        minetest.get_node_timer(pos):start(10)
        --local meta = minetest.get_meta(pos)
        --meta:set_string("infotext", 'Cosechadora: Funcionando')
        minetest.get_meta(pos):set_string("mantener_temporizador", 's')
        --mcempv.cosechadora.infotext(pos, 'Funcionando')
        mcempv.cosechadora.on_timer(pos)
    end
end


mcempv.cosechadora.temporizador_parar = function(pos)
    if minetest.get_node_timer(pos):is_started() then
        minetest.get_node_timer(pos):stop()
    end
end


minetest.register_craft({
    output = "mcempv:cosechadora",
    recipe = {
        {"farming:hoe_steel", "mesecons_luacontroller:luacontroller0000", "farming:hoe_steel"}
    }
})


minetest.register_node("mcempv:cosechadora", {
	description = S("Cosechadora"),
	drawtype = "nodebox",
    tiles = {
          "mcempv_cosechadora_top.png",    -- TOP
          "mcempv_cosechadora_bot.png",  -- BOT
          "mcempv_cosechadora_left.png",  -- LEFT
          "mcempv_cosechadora_right.png",  -- RIGHT
          "mcempv_cosechadora_front.png",  -- FRONT
          "mcempv_cosechadora_back.png",  -- BACK
        },

    groups = {choppy = 2, oddly_breakable_by_hand = 1},

    on_construct = function(pos, node)
        --minetest.log(dump(farming))
        local meta = minetest.get_meta(pos)

        meta:from_table({
            inventory = {
                --[[parcela = {[1] = "",    [2] = "",  [3] = "",  [4] = "",  [5] = "",  [6] = "",  [7] = "",
                           [8] = "",    [9] = "", [10] = "", [11] = "", [12] = "", [13] = "", [14] = ""},]]--
                parcela = mcempv.inventario.crear_vacio(14),
                --[[cajon =   {[1] = "",    [2] = "",  [3] = "",  [4] = "",  [5] = "",  [6] = "",  [7] = "",
                           [8] = "",    [9] = "", [10] = "", [11] = "", [12] = "", [13] = "", [14] = "",
                           [15] = "",  [16] = "", [17] = "", [18] = "", [19] = "", [20] = "", [21] = "",
                           [22] = "",  [23] = "", [24] = "", [25] = "", [26] = "", [27] = "", [28] = ""},]]--
                cajon = mcempv.inventario.crear_vacio(28),
                biofuel = mcempv.inventario.crear_vacio(1)
            },
            fields = {
                formspec = mcempv.cosechadora.formspec(1),
                infotext = "Cosechadora: Parada"
            }
        })
        meta:set_string("direccion", 'Norte')
        meta:set_string("mantener_temporizador", 'n')
        meta:set_int("biofuel", 0)
        mcempv.cosechadora.infotext(pos, 'Parada')
        mcempv.cosechadora.get_inventario(pos)
        --minetest.get_node_timer(pos):start(10)
        --minetest.log(dump(meta:to_table()))
    end,


    on_receive_fields = function(pos, formname, fields, sender)
        if type(fields['mcempv_cosechadora_Direccion'])~='nil' then
            --local meta = minetest.get_meta(pos)
            minetest.get_meta(pos):set_string("direccion", fields['mcempv_cosechadora_Direccion'])
            minetest.get_meta(pos):set_string("formspec", mcempv.cosechadora.formspec(fields['mcempv_cosechadora_Direccion']))
        end
    end,

    on_metadata_inventory_put = function(pos, inv_nombre, inv_id, item_stack, player)
        if inv_nombre=='biofuel' and item_stack:get_name()=="biofuel:fuel_can" then
            if minetest.get_meta(pos):get_int('biofuel')<1 then
                mcempv.cosechadora.inventario_sacar(pos, 'biofuel', 1, 1)
                minetest.get_meta(pos):set_int('biofuel', 300)
            end
        end
        if inv_nombre=='parcela' or inv_nombre=='biofuel' then
            mcempv.cosechadora.temporizador_activar(pos)
        end
        --minetest.log(dump(datos))
    end,

    on_timer = function(pos, elapsed)
        return mcempv.cosechadora.on_timer(pos)
    end,

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        --minetest.chat_send_player(clicker:get_player_name(), S("Click derecho"))
        --minetest.log("Click derecho")
        --local meta = minetest.get_meta(pos)
        --minetest.log(dump(meta:to_table()))

        --local inv_nodo = mcempv.cosechadora.meta:get_inventory({ type="node", pos=pos })
        --minetest.log(dump(inv_nodo))
        
        --[[minetest.log(dump(inv_nodo:get_stack('parcela', 1)))
        minetest.log(mcempv.cosechadora.inventario('parcela', pos))
        minetest.log(inv_nodo:get_stack('parcela', 1):get_count())

        minetest.log(dump(inv_nodo:get_stack('parcela', 2)))
        minetest.log(inv_nodo:get_stack('parcela', 2):get_name())
        minetest.log(inv_nodo:get_stack('parcela', 2):get_count())
        --local inv = meta:get_inventory()
        --inv:set_size("main", 8*4)]]--
        return nil
    end,
})









mcempv.barril = {}


mcempv.barril.que_guarda = function(pos)
    return minetest.get_meta(pos):get_string("mcempv:queGuarda")
end


mcempv.barril.que_guarda_nombre = function(pos)
    local nombre = minetest.get_meta(pos):get_string("mcempv:queGuarda")

    if nombre=='mobs:bucket_milk' then
        return 'Leche'
    elseif nombre=='ethereal:bucket_cactus' then
        return 'Pulpa de cactus'
    elseif nombre=='farming:cactus_juice' then
        return 'Jugo de cactus'
    elseif nombre=='vacio' then
        return 'Vacio'
    end
end


mcempv.barril.cantidad = function(pos)
    return minetest.get_meta(pos):get_int("mcempv:cantidad")
end


mcempv.barril.puede_guardarlo = function(nombre) 
    if nombre=='mobs:bucket_milk' then
        return true
    elseif nombre=='ethereal:bucket_cactus' then
        return true
    elseif nombre=='farming:cactus_juice' then
        return true
    else
        return false
    end
end


mcempv.barril.puede_sacar = function(nombre)
    if nombre=='bucket:bucket_empty' then
        return true
    elseif nombre=='vessels:drinking_glass' then
        return true 
    else
        return false
    end
end


mcempv.barril.sacar = function(clicker, itemstack, meta, nombre)
    -- quitamos el cubo vacio
    itemstack:take_item()   
    meta:set_int("mcempv:cantidad", meta:get_int("mcempv:cantidad")-1)

    local inv = clicker:get_inventory()
    local remaining = inv:add_item("main", {name = nombre})
    if not remaining:is_empty() then
        minetest.add_item(clicker:get_pos(), {name = nombre})
    end
end


mcempv.barril.meter = function(clicker, itemstack, meta, nombre)
    local devolver = 'bucket:bucket_empty'
    if nombre=='farming:cactus_juice' then
        devolver = 'vessels:drinking_glass'
    end
    -- quitamos el cubo lleno
    itemstack:take_item()   
    meta:set_int("mcempv:cantidad", meta:get_int("mcempv:cantidad")+1)

    local inv = clicker:get_inventory()
    local remaining = inv:add_item("main", {name = devolver})
    if not remaining:is_empty() then
        minetest.add_item(clicker:get_pos(), {name = devolver})
    end
end


mcempv.barril.limite = 100


minetest.register_craft({
    output = "mcempv:barril",
    recipe = {
        {"group:wood", "group:wood", "group:wood"},
        {"", "default:steel_ingot", "default:steel_ingot"},
        {"group:wood", "group:wood", "group:wood"},
    }
})


minetest.register_node("mcempv:barril", {
	description = S("Barril para guardar líquidos"),
	drawtype = "nodebox",
    tiles = {
          "barril_up.png",    -- TOP
          "barril_down.png",  -- BOT
          "barril_side.png",  -- LEFT
          "barril_side.png",  -- RIGHT
          "barril_side.png",  -- FRONT
          "barril_side.png",  -- BACK
        },

    groups = {choppy = 2, oddly_breakable_by_hand = 1},


    on_construct = function(pos)
        minetest.get_meta(pos):set_string("mcempv:queGuarda", 'vacio')
    end,

    on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
        local meta = minetest.get_meta(pos)
        local nombreObjeto = itemstack:get_name()
        
        if meta:get_int("mcempv:cantidad")==mcempv.barril.limite and not mcempv.barril.puede_sacar(nombreObjeto) then
            minetest.chat_send_player(clicker:get_player_name(), S("¡Barril lleno!"))
            return nil
        end

        if mcempv.barril.puede_guardarlo(nombreObjeto) then
            if 0==mcempv.barril.cantidad(pos) then
                meta:set_string("mcempv:queGuarda", nombreObjeto)
                mcempv.barril.meter(clicker, itemstack, meta, nombreObjeto)
            elseif mcempv.barril.que_guarda(pos)==nombreObjeto then
                mcempv.barril.meter(clicker, itemstack, meta, nombreObjeto)
            end
        elseif mcempv.barril.puede_sacar(nombreObjeto) then
            if meta:get_int("mcempv:cantidad")>0 then
                mcempv.barril.sacar(clicker, itemstack, meta, meta:get_string("mcempv:queGuarda"))
                if meta:get_int("mcempv:cantidad")<=0 then
                    meta:set_string("mcempv:queGuarda", 'vacio')
                end
            end
        end

        meta:set_string("infotext", mcempv.barril.que_guarda_nombre(pos)..': '..meta:get_int("mcempv:cantidad")..'%')
        return nil
    end,


    can_dig = function(pos)
        if minetest.get_meta(pos):get_int("mcempv:cantidad")==0 then
            return true
        else
            return false
        end
    end,
})
