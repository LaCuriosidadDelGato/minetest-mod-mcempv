# MINETEST MOD mcempv

Modificaciones del servidor de minetest [https://mcempv.ovh/](https://mcempv.ovh/)

- Añade la creación de pedernal en el horno

- Crea nodos para mover los biomas

- Crea la cosechadora


## Recursos

- [Imagen semilla](https://www.flaticon.es/icono-gratis/semilla_6266424)

- [Imagen guadaña](https://www.flaticon.es/icono-gratis/guadana_7865657)


