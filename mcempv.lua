local S = minetest.get_translator(minetest.get_current_modname())

-- crea el pedernal en el horno
minetest.register_craft({
    type = "cooking",
    output = "default:flint",
    recipe = "default:gravel",
    cooktime = 30,
})

-- JUNGLA
----------------------------------------------------------
minetest.register_craft({
    output = "mcempv:junglehierba",
    recipe = {
        {"default:junglegrass", "default:junglegrass", "default:junglegrass"},
        {"default:junglegrass", "default:junglegrass", "default:junglegrass"},
        {"farming:seed_cotton", "default:emergent_jungle_sapling", "farming:rice"}
    }
})
minetest.register_node("mcempv:junglehierba", {
    description = S("Hierba de jungla"),
	drawtype = "nodebox",
    --tiles = {"mcempv_junglefardo.png"},
    tiles = {"ethereal_grass_jungle_top.png"},
    groups = {choppy = 2, oddly_breakable_by_hand = 1},
    paramtype = "light"
})
minetest.register_craft({
    output = "ethereal:jungle_dirt",
    recipe = {
        {"mcempv:junglehierba"},
        {"composting:garden_soil"}
    }
})


-- ARBOLEDA
----------------------------------------------------------
minetest.register_craft({
    output = "mcempv:fernfardo",
    recipe = {
        {"ethereal:fern", "ethereal:fern", "ethereal:fern"},
        {"ethereal:fern", "ethereal:fern", "ethereal:fern"},
        {"ethereal:lemon_tree_sapling", "ethereal:banana_tree_sapling", "ethereal:olive_tree_sapling"}
    }
})
minetest.register_node("mcempv:fernfardo", {
    description = S("Hierba de arboleda"),
	drawtype = "nodebox",
    --tiles = {"mcempv_fernfardo.png"},
    tiles = {"ethereal_grass_grove_top.png"},
    groups = {choppy = 2, oddly_breakable_by_hand = 1},
    paramtype = "light"
})
minetest.register_craft({
    output = "ethereal:grove_dirt",
    recipe = {
        {"mcempv:fernfardo"},
        {"composting:garden_soil"}
    }
})

-- BAMBU
----------------------------------------------------------
minetest.register_craft({
    output = "mcempv:bushfardo",
    recipe = {
        {"ethereal:bush", "ethereal:bush", "ethereal:bush"},
        {"ethereal:bush", "ethereal:bush", "ethereal:bush"},
        {"ethereal:bamboo_sprout", "ethereal:sakura_sapling", ""}
    }
})
minetest.register_node("mcempv:bushfardo", {
    description = S("Hierba de bamboo"),
	drawtype = "nodebox",
    --tiles = {"mcempv_bushfardo.png"},
    tiles = {"ethereal_grass_bamboo_top.png"},
    groups = {choppy = 2, oddly_breakable_by_hand = 1},
    paramtype = "light"
})

minetest.register_craft({
    output = "ethereal:bamboo_dirt",
    recipe = {
        {"mcempv:bushfardo"},
        {"composting:garden_soil"}
    }
})

-- FIERY TIERRA ROJA
----------------------------------------------------------
minetest.register_craft({
    output = "mcempv:dry_shrubfardo",
    recipe = {
        {"ethereal:dry_shrub", "ethereal:dry_shrub", "ethereal:dry_shrub"},
        {"ethereal:dry_shrub", "ethereal:dry_shrub", "ethereal:dry_shrub"},
        {"", "ethereal:fire_flower", ""}
    }
})
minetest.register_node("mcempv:dry_shrubfardo", {
    description = S("Hierba de fuego"),
	drawtype = "nodebox",
    --tiles = {"mcempv_dry_shrubfardo.png"},
    tiles = {"ethereal_grass_fiery_top.png"},
    groups = {choppy = 2, oddly_breakable_by_hand = 1},
    paramtype = "light"
})
minetest.register_craft({
    output = "ethereal:fiery_dirt",
    recipe = {
        {"mcempv:dry_shrubfardo"},
        {"composting:garden_soil"}
    }
})


-- TIERRA DE HONGOS
----------------------------------------------------------
minetest.register_craft({
    output = "ethereal:mushroom_dirt",
    recipe = {
        {"ethereal:mushroom", "ethereal:mushroom_trunk", "ethereal:mushroom"},
        {"ethereal:mushroom_red", "ethereal:mushroom_sapling", ""},
        {"", "composting:garden_soil", ""}
    }
})


-- PRAIRIE PRADERA
----------------------------------------------------------
minetest.register_craft({
    output = "mcempv:pine_bush_needlesfardo",
    recipe = {
        {"default:pine_bush_needles", "default:pine_bush_needles", "default:pine_bush_needles"},
        {"default:pine_bush_needles", "default:pine_bush_needles", "default:pine_bush_needles"},
        {"default:pine_bush_sapling", "ethereal:orange_tree_sapling", "flowers:dandelion_yellow"}
    }
})
minetest.register_node("mcempv:pine_bush_needlesfardo", {
    description = S("Hierba de pradera"),
	drawtype = "nodebox",
    --tiles = {"mcempv_pine_bush_needlesfardo.png"},
    tiles = {"ethereal_grass_prairie_top.png"},
    groups = {choppy = 2, oddly_breakable_by_hand = 1},
    paramtype = "light"
})
minetest.register_craft({
    output = "ethereal:prairie_dirt",
    recipe = {
        {"mcempv:pine_bush_needlesfardo"},
        {"composting:garden_soil"}
    }
})
